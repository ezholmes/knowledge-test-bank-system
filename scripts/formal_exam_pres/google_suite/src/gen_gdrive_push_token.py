#!/usr/bin/python3
import argparse
import os
import pickle
from google_auth_oauthlib.flow import InstalledAppFlow

description = "This will create a token.pickle for use in your pipeline. Run this locally if you need a new " \
              "token.pickle. Login and generate a token.pickle for access. It will redirect you to a login page " \
              "and provide you with new credentials. The generated file should not be saved in gitLab; instead, " \
              "create a masked pipeline variable and pass it to google_drive_uploader.py."
credentials_file_path_desc = "The path to your credentials.json file."
out_dir_default = "."
out_dir_desc = f"The folder to store your new token.pickle file. The default is '{out_dir_default}'"
scopes_default = [
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.file"
]
scopes_desc = "The needed scopes for accessing Google API services."
token_file_name_default = "gdrive_images_token.pickle"
token_file_name_desc = f"The name of the generated token.pickle file. The default is '{token_file_name_default}'"


def find_path(file):
    for root, dirs, files in os.walk('.'):
        for name in files:
            if name == file:
                return os.path.abspath(os.path.join(root, name))


def generate_token(args: argparse.Namespace):
    creds = None
    token_path = os.path.join(args.out_dir, args.token_file_name)
    
    try:
        if not os.path.exists(args.credentials_file_path):
            args.credentials_file_path = find_path(os.path.basename(args.credentials_file_path))
        flow = InstalledAppFlow.from_client_secrets_file(args.credentials_file_path, args.scopes)
        creds = flow.run_local_server(port=0)
    except Exception as e:
        print("The google credentials file is not found, please ensure you have a credentials.json file."
              f"The actual error is: {e}")
    else:
        # Save the credentials for future runs
        with open(token_path, 'wb') as token:
            pickle.dump(creds, token)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description)
    parser.add_argument("credentials_file_path", type=str, help=credentials_file_path_desc)
    parser.add_argument("--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("--scopes", nargs='+', type=str, default=scopes_default, help=scopes_desc)
    parser.add_argument("--token_file_name", type=str, default=token_file_name_default, help=token_file_name_desc)
    params = parser.parse_args()
    generate_token(params)
