# Forking the Project
The first step in setting up a new Knowledge Test Bank System is to fork the Knowledge Test Bank Template.

- Follow GitLab's [Forking instructions](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) to create a Fork of the [Knowledge Test Bank Template](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system).
