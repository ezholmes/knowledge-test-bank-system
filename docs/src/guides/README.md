## Purpose
These guides are meant for exam administrators and provide direction on various setup procedures for examinations. The sections in this chapter are organized in a logical manor to help you step through the complete setup process.
