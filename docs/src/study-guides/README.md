# Table of Contents
- [Study Guides](#study-guides)
  - [Navigating to the Study Guides](#navigating-to-the-study-guides)
  - [Usage Information](#usage-information)

# Study Guides
This will explain how to access and use the Study Guides.

\[ [TOC](#table-of-contents) \]

## Navigating to the Study Guides
To access the Study Guides:
- Using your favorate web browser, navigate to the knowledge test bank repository.
- Click the link at the top of the page, see figure 1 below.

Figure 1: Project Description Location
![fig 1](images/fig_1_study-guides.png)

\[ [TOC](#table-of-contents) \]

## Usage Information
Once you reach the repository's documentation Markdown book, you will see a navigation menu on the left and content on the right, refer to figure 2 below. The menu on the left is essentially a table of contents for the book. To study for a particular work-role, do the following.
- Under the `Study Banks` chapter, click on the desired work-role.
- In the content view on the right, a table of contents displays all of the available topics for this work-role
- Choose a topic to begin your study
- Read each question carefully and try to come up with the correct answer.
- When you are ready, click on the `eye` icon to reveal the answer.
- To hide the answer, click on the `eye` icon again.
- A link back to the table of contents is provided at the beginning of each topic.

Figure 2: Study Guides Markdown Book
![fig 2](images/fig_2_study-guides.png)

\[ [TOC](#table-of-contents) \]
