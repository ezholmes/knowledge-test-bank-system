# System Conceptual Overview

### Before we Begin
There are a few things that need to be clarified. 
1. This is an all encompassing product using many different open source technologies
2. These technologies are used in different layers of the system.
3. The ideal goal is control!


## 1. Initial Requirements
The following is a list of requirements that we have used to design this system
1. We need to incorporate some sort of version control
    - Enables us to create change management workflows for every aspect of the product
    - Make change quickly with low risk
3. We need the ability to deploy study banks
    - The study material will include the question and solution
    - The solution should not immidiately be shown to the user
4. We need the ability to deploy an official examination
    - The examination should be able to be taken remotely
    - Statistics for each examination we deploy should be produced and stored automatically


## 2. The Concept
Before we get into the different technologies used in this product lets first discuss the different layers.

How do we define a layer? A layer is a feature separated subsystem. This means that if it is decided that a layer no longer meets our needs, we can dedicate development time to augment that layer to better fit our requirements, without affecting the other layers.

This product has two layers.
1. Test Bank
2. Study Presentation
3. Formal Exam Presentation


## 3. The Layers
### Test Bank
The test bank(s) are where the literal exam material will reside. Each question will be isolated in its own file, which can be changed and submitted for approval. This layer is also responsible for storing any and all statistical information for exam material.

### Study Presentation
The study presentation layer is specifically for the public study bank presentation for members of the organization. This layer houses all knowledge system documentation and the study banks for all defined test banks.

### Formal Exam Presentation
The formal exam presentation layer provides the means to deploy official knowledge exams and present them at a moment's notice.


## 4. The Technology
### Test Bank
The test bank is really just where the material resides. We have chosen to use a [git](https://git-scm.com/) repository as our version control system to maintain the material. With this we're using [GitLab](https://about.gitlab.com/what-is-gitlab/) for change and project management along with [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) for validation and testing of material.

### Study Presentation
The study presentation is hosted by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). This GitLab feature is used to host a static website of our design. We're using this technology to host a [MDBook](https://github.com/rust-lang/mdBook) which is dynamically populated with the test material from the `Test Bank`. In the CI/CD pipeline, we're using Python 3 to create the presentation of the material in the mdbook.

### Formal Exam Presentation
For the time being, the formal knowledge exams are using [Google Forms](https://www.google.com/forms/about/) as a testing platform. Google Forms provides exam by exam statistics but as of now, there are no automated ways to gather and present exam material trends over multiple exams. Right now, the CI/CD pipeline creates a `csv` that we manually put into a Google Drive. From here we generate the Google Form from this `csv`. 

In the future, no interaction with Google products will be necessary. The CI/CD will create the Google Form automatically and present the link to destribute to the examinees. We are also planning on adding automated [LimeSurvey](https://github.com/LimeSurvey/LimeSurvey) deployment. Both of these will be options that the organization can choose.
