
**INTRODUCTION:**  Good morning.  My name is ___________ and I am from the 90 COS Evaluations Element.  I will be proctoring your exam today.  Assisting me is ___________.  The exam will begin at the end of this inbrief. You should have no other electronic devices with you except for the examination workstations.

**TECHNICAL ISSUES:**  If you experience any technical difficulties, Direct Message one of the proctors and you will be taken to another discussion area to solve the issue.  You are also authorized to ask Proctors questions related to the exam.  Understand we will only answer to clarify; we will not provide any information that will assist you with the solution. Don't hesitate to ask questions pertaining to the location of files or tools. 

**PRE-EVAL FORM:**
Before you begin the evaluation fill out the pre-evaluation form. 

 
**DESCRIPTION AND PROCEDURES:**

This exam you are about to take is a requirement.  You must achieve an 80% or better score to be permitted to take the Performance Phase exam.  Since this is a “closed book, closed Internet” exam, you are NOT permitted to browse or search the Internet.  You are permitted, however, to use the calculator on your phone or computer, if needed.

You will have 90 minutes to answer all multiple choice questions.  Remember to take your time and pay attention to the details in the answer options! Make sure you double-check to ensure you've answered all the questions before you submit your exam.



**RESULTS:**

When your exam is complete, click “Submit.”  You will immediately see your score, and any questions you missed along with the correct answers to the missed questions.  

FEEDBACK:  Within one to two business days following this exam, you will receive a short feedback form via e-mail.  Please take a few minutes to fill this out so it will help the Evaluations Element improve their service to the squadron.

BEGIN EXAMINATION: 
Are there any final questions?
The time is <time-hack>.  You may begin your exam.
