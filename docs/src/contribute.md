# Contributing to Test Banks

Make sure to fork the test bank repo and read through the [CONTRIBUTE.md](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/blob/master/CONTRIBUTE.md) file.